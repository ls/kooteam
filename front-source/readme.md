# 环境配置
+ 安装nodejs
+ 安装ruby

# 依赖包安装
+ npm install -g grunt-cli
+ npm install -d
+ gem install sass

# 启动编译环境
+ 启动前端调试 grunt start:kooteam
+ 前端源码编译 grunt build:kooteam
+ lib 前端第三方依赖目录
