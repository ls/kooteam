# 产品介绍
+ kooteam是一款轻量级的在线团队协作工具，提供各类文档工具、在线思维导图、在线流程图、项目管理、任务分发，知识库管理等工具。
+ kooteam自动同步钉钉企业通讯录，实现钉钉扫码自动登陆，权限与钉钉同步，100%保障数据安全。

## 技术选型
+ 后端框架：[Blade MVC](https://github.com/lets-blade/blade)
+ 前端框架：[Vue 2.0](https://cn.vuejs.org/)
+ 数据库：Mysql
+ web服务器：nginx（开发与运行环境都依赖）

## 相关网址
+ 个人版在线体验地址：[https://www.kooteam.com](https://www.kooteam.com)
+ [安装教程](https://www.kooteam.com/view.html?id=5bf35ce443880a6558827185)

## 功能简介
**1. 待办四象限：突出事情优先级，帮助员工合理安排时间，提高工作效率**
![待办四象限：突出事情优先级，帮助员工合理安排时间，提高工作效率](https://b.yimiyisu.com/kooteam/banner/todo.jpg)

**2. 在线流程图：在线流程图工具，使用方便**
![在线流程图：在线流程图工具，使用方便](https://b.yimiyisu.com/kooteam/banner/1.jpg)

**3. 在线思维导图：梳理思路，优化工作流程**
![在线思维导图：梳理思路，优化工作流程](https://b.yimiyisu.com/kooteam/banner/2.jpg)


**4. 项目管理：自定义项目看板，可视化任务安排**
![项目管理：自定义项目看板，可视化任务安排](https://b.yimiyisu.com/kooteam/banner/project.jpg)

**5. 在线知识库：在线流程图，在线文档，以及可视化的目录编排，文档管理无忧**
![在线知识库：在线流程图，在线文档，以及可视化的目录编排，文档管理无忧](https://b.yimiyisu.com/kooteam/banner/wiki.jpg)

## 前端开源说明

kooteam前端代码基于公司内部云端环境研发，但代码的编译与发布均在云端自动完成，由于前端自动化工具还没有实现本地化，我们预计2019年4月份，前端代码全部开源。

